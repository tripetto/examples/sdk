import definitionJson from "./definition.json";
import stylesJson from "./styles.json";
import translationJson from "tripetto-runner-autoscroll/runner/translations/nl.json";
import { run } from "tripetto-runner-autoscroll";
import { Export, IDefinition, TStyles, L10n } from "tripetto-runner-foundation";

run({
    element: document.body,
    definition: definitionJson as IDefinition,
    styles: stylesJson as TStyles,
    translations: translationJson as {} as L10n.TTranslation,
    onSubmit: (instance) =>
        new Promise((resolve: (id?: string) => void, reject: (reason?: string) => void) => {
            // The form is completed, we now can process the data.

            // Output the exportable data to the console for demo purposes.
            console.dir(Export.exportables(instance));

            // Output can also be exported as CSV for your convenience.
            console.dir(Export.CSV(instance));

            resolve(); // Use `reject` here if there was a problem.
        }),
});
