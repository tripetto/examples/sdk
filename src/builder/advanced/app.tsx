import React from "react";
import ReactDOM from "react-dom";
import { Builder, IBuilderChangeEvent, IBuilderReadyEvent, IBuilderEditEvent, pgettext, mountNamespace, unmountNamespace } from "tripetto";
import { IDefinition, TStyles } from "tripetto-runner-foundation";
import { Header } from "./header";
import { run as runAutoscroll } from "tripetto-runner-autoscroll";
import { run as runChat } from "tripetto-runner-chat";
import { run as runClassic } from "tripetto-runner-classic";

// Styles contracts
import autoscrollStylesContract from "tripetto-runner-autoscroll/builder/styles";
import chatStylesContract from "tripetto-runner-chat/builder/styles";
import classicStylesContract from "tripetto-runner-classic/builder/styles";

// L10n contracts
import autoscrollL10nContract from "tripetto-runner-autoscroll/builder/l10n";
import chatL10nContract from "tripetto-runner-chat/builder/l10n";
import classicL10nContract from "tripetto-runner-classic/builder/l10n";

// Styles data
import autoscrollStylesData from "../../runner/corona-screening/autoscroll/styles.json";
import chatStylesData from "../../runner/corona-screening/chat/styles.json";
import classicStylesData from "../../runner/corona-screening/classic/styles.json";

// Example definitions
import coronaScreeningDefinition from "../../runner/corona-screening/autoscroll/definition.json";
import orderFormDefinition from "../../runner/order-form/autoscroll/definition.json";

// This demo implements multiple runners than run simultaneously. Each runner
// comes with a bundle with the implemented builder blocks for that runner.
// Here we load those bundles in different namespaces. By activating the
// appropriate namespace during runtime in the builder we can switch between
// builder block bundles. This allows for differences between the blocks used
// in the runners.
mountNamespace("autoscroll");
import "tripetto-runner-autoscroll/builder";
unmountNamespace();

mountNamespace("chat");
import "tripetto-runner-chat/builder";
unmountNamespace();

mountNamespace("classic");
import "tripetto-runner-classic/builder";
unmountNamespace();

// Create a new builder instance.
const builder = Builder.open(undefined, {
    element: document.getElementById("builder"),
    fonts: "../../assets/fonts/",
    disableLogo: true,
    disableEditButton: true,
    disableSaveButton: true,
    disableRestoreButton: true,
    disableTutorialButton: true,
    disableClearButton: true,
    disableCloseButton: true,
    disableOpenCloseAnimation: true,
    supportURL: false,
    zoom: "fit-horizontal",
});

// Wait until the builder is ready!
builder.hook("OnReady", "synchronous", async (builderEvent: IBuilderReadyEvent) => {
    let activeRunner: "autoscroll" | "chat" | "classic" = "autoscroll";

    // Get the DOM elements for the runners.
    const elements = {
        autoscroll: document.getElementById("autoscroll")!,
        chat: document.getElementById("chat")!,
        classic: document.getElementById("classic")!,
    };

    // When a change is made in the builder, the runner is updated so you have a live preview.
    const updateRunner = (definition: IDefinition) => {
        switch (activeRunner) {
            case "autoscroll":
                autoscrollRunner.definition = definition;
                break;
            case "chat":
                chatRunner.definition = definition;
                break;
            case "classic":
                classicRunner.definition = definition;
                break;
        }
    };

    // The builder has a styles editor than can be used to manage the styles of the runners.
    const editStyles = () => {
        switch (activeRunner) {
            case "autoscroll":
                builder.stylesEditor(autoscrollStylesContract(pgettext), autoscrollRunner.styles, "premium", (styles) => autoscrollRunner.styles = styles);
                break;
            case "chat":
                builder.stylesEditor(chatStylesContract(pgettext), chatRunner.styles, "premium", (styles) => chatRunner.styles = styles);
                break;
            case "classic":
                builder.stylesEditor(classicStylesContract(pgettext), classicRunner.styles, "premium", (styles) => classicRunner.styles = styles);
                break;
        }
    };

    // The builder has a l10n editor than can be used to set language/translation oriented settings.
    const editL10n = () => {
        switch (activeRunner) {
            case "autoscroll":
                builder.l10nEditor(autoscrollL10nContract(), autoscrollRunner.l10n, (l10n) => autoscrollRunner.l10n = l10n);
                break;
            case "chat":
                builder.l10nEditor(chatL10nContract(), chatRunner.l10n, (l10n) => autoscrollRunner.l10n = l10n);
                break;
            case "classic":
                builder.l10nEditor(classicL10nContract(), classicRunner.l10n, (l10n) => autoscrollRunner.l10n = l10n);
                break;
        }
    };

    // This function is invoked when a runner is ready or when the active runner is changed.
    const onReady = () => {
        builder.useNamespace(activeRunner);

        elements.autoscroll.classList.toggle("visible", activeRunner === "autoscroll");
        elements.chat.classList.toggle("visible", activeRunner === "chat");
        elements.classic.classList.toggle("visible", activeRunner === "classic");
    };

    // This function is invoked when an item is clicked in the runner. It will open the appropriate item in the builder.
    const onEdit = (type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => {
        switch (type) {
            case "prologue":
                builder.edit("prologue");
                break;
            case "epilogue":
                builder.edit("epilogue", id);
                break;
            case "styles":
                    editStyles();
                    break;
                case "l10n":
                    editL10n();
                    break;
            case "block":
                if (id) {
                    builder.edit("node", id);
                }
                break;
        }
    };

    // Let's render an autoscroll runner.
    const autoscrollRunner = await runAutoscroll({
        element: elements.autoscroll,
        definition: builderEvent.definition,
        styles: autoscrollStylesData as TStyles,
        view: "test",
        onReady,
        onEdit
    });

    // Let's render a chat runner.
    const chatRunner = await runChat({
        element: elements.chat,
        definition: builderEvent.definition,
        styles: chatStylesData as TStyles,
        view: "test",
        onReady,
        onEdit
    });

    // Let's render a classic runner.
    const classicRunner = await runClassic({
        element: elements.classic,
        definition: builderEvent.definition,
        styles: classicStylesData as TStyles,
        view: "test",
        onReady,
        onEdit
    });

    // Let's render a header component
    ReactDOM.render(
        <Header
            builder={builder}
            activeRunner={activeRunner}
            editStyles={editStyles}
            editL10n={editL10n}
            onLoadCoronaScreening={() => builder.load(coronaScreeningDefinition as IDefinition)}
            onLoadOrderForm={() => builder.load(orderFormDefinition as IDefinition)}
            onClear={() => builder.clear()}
            onRestart={() => {
                autoscrollRunner.restart();
                chatRunner.restart();
                classicRunner.restart();
            }}
            onRunnerChange={(runner) => {
                if (runner !== activeRunner) {
                    activeRunner = runner;

                    onReady();

                    if (builder.definition) {
                        updateRunner(builder.definition);
                    }
                }
            }}
            onViewChange={(view) => {
                autoscrollRunner.view = view;
                chatRunner.view = view;
                classicRunner.view = view;
            }}
        />,
        document.getElementById("header")
    );

    // When the definition in the builder is changed, update the runners.
    builder.hook("OnChange", "synchronous", (changeEvent: IBuilderChangeEvent) => updateRunner(changeEvent.definition));

    // When the user starts editing an item we can try to scroll that item into view.
    builder.hook("OnEdit", "synchronous", (editEvent: IBuilderEditEvent) => {
        autoscrollRunner.doPreview(editEvent.data);
        chatRunner.doPreview(editEvent.data);
        classicRunner.doPreview(editEvent.data);
    });

    // When the host window resizes, we should notify the builder.
    window.addEventListener("resize", () => builder.resize());
    window.addEventListener("orientationchange", () => builder.resize());
});
