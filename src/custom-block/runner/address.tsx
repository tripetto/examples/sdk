import { NodeBlock, Slots, assert, tripetto } from "tripetto-runner-foundation";
import { IAutoscrollRenderProps, IAutoscrollRendering, namespace, styled } from "tripetto-runner-autoscroll";
import Geosuggest, { Suggest } from "react-geosuggest";

@tripetto({
    /** This binds the autoscroll runner namespace to the block. */
    namespace,

    /** This is a node block. */
    type: "node",

    /** This is the unique type identifier for the block. */
    identifier: "address",
})
export class Address extends NodeBlock implements IAutoscrollRendering {
    readonly addressSlot = assert(this.valueOf<string, Slots.String>("address"));
    readonly required = this.addressSlot.slot.required || false;

    render(props: IAutoscrollRenderProps): React.ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <GeosuggestElement
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    onChange={(address: string) => {
                        this.addressSlot.value = address;
                    }}
                    onSuggestSelect={(suggest?: Suggest) => {
                        this.addressSlot.value = suggest?.label || "";
                    }}
                />
            </>
        );
    }
}

// We use styled-components to style the geosuggest component.
export const GeosuggestElement = styled(Geosuggest)`
    > div:first-child {
        > input {
            appearance: none;
            outline: none;
            box-sizing: border-box;
            display: block;
            width: 100%;
            font-size: 1em;
            line-height: 1.5em;
            background-color: white;
            background-repeat: no-repeat;
            background-size: ${8 / 7}em;
            background-position: right 0.375em center;
            border: 1px solid black;
            color: black;
            border-radius: 0.5em;
            padding: 0.375em 0.75em;
            margin: 0;
            height: 45px;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out,
                box-shadow 0.15s ease-in-out, opacity 0.15s ease-in-out;
        }
    }

    > div:last-child {
        > ul {
            padding: 0;
            margin: 0;
            border-radius: 0.5em;

            li {
                list-style: none;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 1e,;
                color: #fff;
                border-bottom: 1px solid #fff;
                background-color: #000;
                padding: 10px;
            }
        }

        .geosuggest__suggests--hidden {
            max-height: 0;
            overflow: hidden;
            border-width: 0;
        }

        .geosuggest__item--active {
            background: #1c7cd6;
            color: #fff;
        }
    }
`;
