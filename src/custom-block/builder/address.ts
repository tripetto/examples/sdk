import { NodeBlock, Slots, editor, _, slots, tripetto } from "tripetto";
import ICON from "./address.svg";

@tripetto({
    /** This is a node block. */
    type: "node",

    /** This is the unique type identifier for the block. */
    identifier: "address",

    /** An icon for the block. */
    icon: ICON,

    /** A localized label for the block. */
    get label() {
        return _("Address (with autocomplete)");
    },
})
export class Address extends NodeBlock {
    addressSlot!: Slots.String;

    @slots
    defineSlot(): void {
        this.addressSlot = this.slots.static({
            type: Slots.String,
            reference: "address",
            label: Address.label,
            exchange: ["required", "alias", "exportable"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(this.addressSlot);
        this.editor.visibility();
        this.editor.alias(this.addressSlot);
        this.editor.exportable(this.addressSlot);
    }
}
