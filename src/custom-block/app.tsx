import React from "react";
import ReactDOM from "react-dom";
import { Builder, IBuilderChangeEvent, IBuilderReadyEvent, IBuilderEditEvent, pgettext } from "tripetto";
import { TStyles } from "tripetto-runner-foundation";
import { Header } from "./header";
import { run } from "tripetto-runner-autoscroll";
import autoscrollStylesContract from "tripetto-runner-autoscroll/builder/styles";
import autoscrollL10nContract from "tripetto-runner-autoscroll/builder/l10n";
import autoscrollStylesData from "../runner/corona-screening/autoscroll/styles.json";
import "tripetto-runner-autoscroll/builder";
import "./builder/address";
import "./runner/address";

// Create a new builder instance.
const builder = Builder.open(undefined, {
    element: document.getElementById("builder"),
    fonts: "../../assets/fonts/",
    disableLogo: true,
    disableEditButton: true,
    disableSaveButton: true,
    disableRestoreButton: true,
    disableTutorialButton: true,
    disableClearButton: true,
    disableCloseButton: true,
    disableOpenCloseAnimation: true,
    supportURL: false,
    zoom: "fit-horizontal",
});

// Wait until the builder is ready!
builder.hook("OnReady", "synchronous", async (builderEvent: IBuilderReadyEvent) => {
    // Let's render an autoscroll runner.
    const runner = await run({
        element: document.getElementById("runner"),
        definition: builderEvent.definition,
        styles: autoscrollStylesData as TStyles,
        view: "test",
        onEdit: (type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => {
            switch (type) {
                case "prologue":
                    builder.edit("prologue");
                    break;
                case "epilogue":
                    builder.edit("epilogue", id);
                    break;
                case "block":
                    if (id) {
                        builder.edit("node", id);
                    }
                    break;
            }
        },
    });

    // Let's render a header component
    ReactDOM.render(
        <Header
            builder={builder}
            editStyles={() =>
                builder.stylesEditor(autoscrollStylesContract(pgettext), runner.styles, "premium", (styles) => (runner.styles = styles))
            }
            editL10n={() => builder.l10nEditor(autoscrollL10nContract(), runner.l10n, (l10n) => (runner.l10n = l10n))}
            onClear={() => builder.clear()}
            onRestart={() => runner.restart()}
            onViewChange={(view) => (runner.view = view)}
        />,
        document.getElementById("header")
    );

    // When the definition in the builder is changed, update the runners.
    builder.hook("OnChange", "synchronous", (changeEvent: IBuilderChangeEvent) => (runner.definition = changeEvent.definition));

    // When the user starts editing an item we can try to scroll that item into view.
    builder.hook("OnEdit", "synchronous", (editEvent: IBuilderEditEvent) => runner.doPreview(editEvent.data));

    // When the host window resizes, we should notify the builder.
    window.addEventListener("resize", () => builder.resize());
    window.addEventListener("orientationchange", () => builder.resize());
});
