This PoC demonstrates how to implement [Tripetto](https://tripetto.com), a framework for building and running advanced forms and surveys. It contains example forms (a corona screening and an order form) that can serve as a demo application.

# How Tripetto works
Tripetto consists of two main components: The ***builder*** and the ***runner***. The builder is for creating forms, and the runner runs those forms and collects the actual results from the respondents. There is no dependency on a particular server application/backend or third-party library, and the components work very well in stateless environments, reducing application complexity.

## The builder: *Revolutionizing the way people create forms*
Tripetto has a unique approach to creating forms. Instead of managing a flat list of questions (making it hard to work with logic flows), it uses a self-organizing drawing board that allows users to create smart forms with visual logic and conditional flows. It works in any modern browser and with mouse, touch, or pen (full support for tablets!). The complete structure of a form is stored in a JSON format: The form definition. The builder component itself is a pure JavaScript component delivered though [npm](https://www.npmjs.com/package/tripetto) and is easy to implement in custom applications.

## The runner: *Run awesome form experiences in stateless environments*
Not only for the builder, Tripetto has a unique approach, but also for the runners used to collect data. The base runner component (also delivered through [npm](https://www.npmjs.com/package/tripetto-runner-foundation)) has no UI at all. Instead, it turns a form definition (created with the builder) into an executable program, a finite state machine that handles all the complex logic and response collection during the execution of the form. Then any UI framework can be applied on top of that to build an actual visible form.

There are many code examples available using various frameworks and libraries, like [React](https://gitlab.com/tripetto/examples/react), [Material-UI](https://gitlab.com/tripetto/examples/react-material-ui), [Angular](https://gitlab.com/tripetto/examples/angular) and [Angular Material](https://gitlab.com/tripetto/examples/angular-material). But besides those examples, the Tripetto-team also maintains three full-featured, stable, and production-ready runner UI's that Tripetto uses in their own end-user applications (like the [Studio web app](https://tripetto.app) and the [WordPress plugin](https://wordpress.org/plugins/tripetto/)). Those runners are:
- ***[Autoscroll runner](https://www.npmjs.com/package/tripetto-runner-autoscroll)***: Uses a scroll effect to display the questions (similar to Typeform's approach);
- ***[Chat runner](https://www.npmjs.com/package/tripetto-runner-chat)***: Displays a form as a chat conversation;
- ***[Classic runner](https://www.npmjs.com/package/tripetto-runner-classic)***: Displays a form in a traditional way.

The visual appearance of the runners (fonts, colors, background, etc.) can be customized either by the developer or - if desired - by the end-user itself. Also, localization (l10n) is supported, allowing the runners to be used in different languages (translations for English and Dutch are included; [more translations are welcome](https://gitlab.com/tripetto/translations)!). The demo applications in this repository all use the above runners because it is the easiest way to get everything up and running.

The runners also support a test and preview mode. In test mode, the form visually works exactly like in a live environment, but no results are stored. The preview mode on the other hand shows all the blocks (by disabling all logic flows), so the user can get a preview of all blocks without the need to fill in the form to view a certain block.

Last but not least, the runners support pausing and resuming. It allows respondents to stop filling in a form and then continue with that form later on (even on another device). Of course, this requires some additional work to get operational and is currently not used in any of the demo applications.

## Blocks (question types): *Bring your own building blocks to the table*
The last key feature of Tripetto is the ability to build custom blocks for the editor and runners. Those blocks can be visible question types (like text input, checkbox, dropdown, etc.), but can also be invisible elements performing specific actions like calculations, API calls, or evaluations. In this repository, there are currently no custom blocks implemented. However, if you want to learn more about this, look at [this list](https://gitlab.com/tripetto/blocks) of open-source building blocks maintained by Tripetto (these are the blocks actually used in the full-featured runners). There is also a [boilerplate](https://gitlab.com/tripetto/blocks/boilerplate) to jumpstart custom block development.

# Overview
![Tripetto JSON Overview](assets/overview.png)

# Structure of this repository
This repository contains a total of 9 demo applications. None of the demos implement a backend that delivers the form definition to the client from a server or stores the actual form results at a server.  Instead, the form definition is fetched during compile time, making it a static asset for demo purposes. For the form results part, nothing is done, other than output it to the local browser console to allow developers to see how Tripetto collects data.

## Demo 1, 2, 3: Corona screening form `./src/runner/corona-screening/`
Implements a corona screening form in all three of the standard Tripetto runners.

## Demo 4, 5, 6: Order form `./src/runner/order-form/`
Implements an order form in all three of the standard Tripetto runners.

## Demo 7: Simple builder application `./src/builder/simple/`
Implements the builder along with a live preview of the form using the Autoscroll runner. Although it's possible to use the builder standalone to create forms, we believe the best user experience is reached by showing a realtime preview next to the builder area. This preview shows the actual form so that the user can see the end result immediately.


*There are two buttons in the header to load the corona screening or order form definition.*

## Demo 8: Advanced builder application `./src/builder/advanced/`
Same as demo 7, but now all three runners are implemented. The user can switch realtime between the runners and see a live preview of the form in a particular runner.

## Demo 9: Custom blocks `./src/custom-block/`
Shows how to build custom blocks for the builder and runner. The example application shows the builder along with a live preview of the form using the Autoscroll runner. There is a custom block `Address` that implements an address control with auto-complete based on the Google Maps Places API.
> To run this demo, make sure to set your Google Maps API key in the file `./public/custom-block/index.html` line #13.

# Demo applications
This repository triggers an automated deploy of the demos on [Gitlab pages](https://gitlab.com/tripetto/examples/sdk/-/environments), making the demos publicly accessible through the following URL's:
- Demo 1: https://tripetto.gitlab.io/examples/sdk/runner/corona-screening/autoscroll.html
- Demo 2: https://tripetto.gitlab.io/examples/sdk/runner/corona-screening/chat.html
- Demo 3: https://tripetto.gitlab.io/examples/sdk/runner/corona-screening/classic.html
- Demo 4: https://tripetto.gitlab.io/examples/sdk/runner/order-form/autoscroll.html
- Demo 5: https://tripetto.gitlab.io/examples/sdk/runner/order-form/chat.html
- Demo 6: https://tripetto.gitlab.io/examples/sdk/runner/order-form/classic.html
- Demo 7: https://tripetto.gitlab.io/examples/sdk/builder/simple/
- Demo 8: https://tripetto.gitlab.io/examples/sdk/builder/advanced/
- Demo 9: https://tripetto.gitlab.io/examples/sdk/custom-block/

# Dev stuff
If you want to run the demos locally, you need [nodeJS](https://nodejs.org/) (version 8 or above is fine). Next, you need to clone this repository and execute the following commands to install the packages and kick-off the dev server:
```bash
$ git clone git@gitlab.com:tripetto/examples/sdk.git
$ cd sdk
$ npm i
$ npm test
```
This will start a development server on `localhost:9000`, making the demos accessible through the following URL's:
- Demo 1: http://localhost:9000/runner/corona-screening/autoscroll.html
- Demo 2: http://localhost:9000/runner/corona-screening/chat.html
- Demo 3: http://localhost:9000/runner/corona-screening/classic.html
- Demo 4: http://localhost:9000/runner/order-form/autoscroll.html
- Demo 5: http://localhost:9000/runner/order-form/chat.html
- Demo 6: http://localhost:9000/runner/order-form/classic.html
- Demo 7: http://localhost:9000/builder/simple/
- Demo 8: http://localhost:9000/builder/advanced/
- Demo 9: http://localhost:9000/custom-block/

*[Webpack](https://webpack.js.org/) is used for generating the JS bundles and assets.*

## Editing bundled forms
If you want to edit the example definitions for the corona screening or order form, you can use the following commands to open a local instance of the Tripetto builder for each demo application. *Make sure to click the save button in Tripetto when you are done editing. This will save the edited definition on disk.*
```bash
$ npm run edit:corona-screening:autoscroll
$ npm run edit:corona-screening:chat
$ npm run edit:corona-screening:classic
$ npm run edit:order-form:autoscroll
$ npm run edit:order-form:chat
$ npm run edit:order-form:classic
```

## Run a production build
To run a production build of the demo applications execute:
```bash
$ npm run make
```

The `./public/` folder contains the build artifacts.

# Building an end-user application
As mentioned earlier, the demo applications don't implement a server backend at all. That's the power of Tripetto since it enables developers to decide where and how to store form definitions and results. By not relying on third-party backend services, it becomes easier to comply with GDPR and ensure system simplicity, scalability, and availability.

So, what needs to be done to build an end-user application? In a nutshell, it includes at least the following ingredients:
1. You need a data store where form definitions can be saved (written) by the builder and fetched (read) by the runners;
2. You need a data store where form results can be saved (written) by the runner and read by any modules that consume form data (export modules, result views for the form owner, etc.);
3. You need to host/implement the builder somewhere so your users can use it;
4. You need to host/implement the runners somewhere so the respondents can access the forms.

The above parts can be developed in a single application on the same domain (Tripetto did that for their Studio application, [source here](https://gitlab.com/tripetto/studio)), but that is certainly not necessary.

# License
The code in this repository is licensed under [MIT](https://opensource.org/licenses/MIT). Have a blast.

