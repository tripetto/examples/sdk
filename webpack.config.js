const path = require("path");
const webpackCopy = require("copy-webpack-plugin");

module.exports = {
    target: ["web", "es5"],
    entry: {
        "builder-simple": "./src/builder/simple/app.tsx",
        "builder-advanced": "./src/builder/advanced/app.tsx",
        "runner-corona-screening-autoscroll": "./src/runner/corona-screening/autoscroll/index.ts",
        "runner-corona-screening-chat": "./src/runner/corona-screening/chat/index.ts",
        "runner-corona-screening-classic": "./src/runner/corona-screening/classic/index.ts",
        "runner-order-form-autoscroll": "./src/runner/order-form/autoscroll/index.ts",
        "runner-order-form-chat": "./src/runner/order-form/chat/index.ts",
        "runner-order-form-classic": "./src/runner/order-form/classic/index.ts",
        "custom-block": "./src/custom-block/app.tsx",
    },
    output: {
        filename: "[name].bundle.js",
        path: __dirname + "/public/assets/js/",
        publicPath: "/assets/js/",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: "ts-loader",
            },
            {
                test: /\.svg$/,
                use: ["url-loader", "image-webpack-loader"],
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
    },
    plugins: [
        new webpackCopy({
            patterns: [{ from: "node_modules/tripetto/fonts/", to: "../fonts/" }],
        }),
    ],
    performance: {
        hints: false,
    },
    devServer: {
        static: path.resolve(__dirname, "public"),
        port: 9000,
        host: "0.0.0.0",
    },
};
